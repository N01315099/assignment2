﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment2a
{
    public partial class Design : System.Web.UI.Page
    {
        DataView CreateCodeSource()
        {
            DataTable codedata = new DataTable();


           
            DataColumn idx_col = new DataColumn();

            
            DataColumn code_col = new DataColumn();

            DataRow coderow;

            idx_col.ColumnName = "Line";
            idx_col.DataType = System.Type.GetType("System.Int32");

            codedata.Columns.Add(idx_col);

            code_col.ColumnName = "Code";
            code_col.DataType = System.Type.GetType("System.String");
            codedata.Columns.Add(code_col);

           
            List<string> design_code = new List<string>(new string[]{
                "Start CSS With",
                "html {",
                    "~box-sizing: border - box;",
                 "}",
                "*, *::before, *::after{",
	            
            });

            int i = 0;
            foreach (string code_line in design_code)
            {
                coderow = codedata.NewRow();
                coderow[idx_col.ColumnName] = i;
                string formatted_code = System.Net.WebUtility.HtmlEncode(code_line);
                formatted_code = formatted_code.Replace("~", "&nbsp;&nbsp;&nbsp;");
                coderow[code_col.ColumnName] = formatted_code;

                i++;
                codedata.Rows.Add(coderow);
            }


            DataView codeview = new DataView(codedata);
            return codeview;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            DataView ds = CreateCodeSource();
            Code_FrontEnd.DataSource = ds;

            /*Some formatting in the codebehind*/


            Code_FrontEnd.DataBind();

        }
    }
}