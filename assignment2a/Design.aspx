﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Design.aspx.cs" Inherits="assignment2a.Design" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Referlinks" runat="server">

<h3 class="useful-links">Helpful Links</h3>
<div class="link-box">
    <p><a href="https://www.w3.org/TR/html52/">HTML elements</a></p>
    <p><a href="https://validator.w3.org/">HTML Validator</a></p>
    <p><a href="https://www.w3.org/TR/html52/">CSS Validator</a></p>
    <p><a href="https://caniuse.com/">Check browser support</a></p>
</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Code" runat="server">
    <h3 class="note-title-background">Notes</h3>
    <p>Google Fonts (https://fonts.google.com/)
This is a huge source of fonts for developers and it's easy to use. You simply go the site, choose the font
you want then use the simple CSS @import statement to add the font to your CSS.
</p>
    <asp:Label runat="server" ID="testlabel">New age</asp:Label>
    <asp:Button runat="server" ID="btntest" text="Know More"/>
    <p>Paste the @import statement at the top of your CSS file. Although you can add the <link> into
your HTML page as an alternative, putting the import statement in your CSS file keeps
everything neat.</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Assignments" runat="server">
    <h3 class="extra-title-background">Assignment Notes</h3>
    <p>HTML Entities are special codes for using non-alphanumeric characters.
        e.g. &amp; = & &lt; = < &gt; = > &copy; = ©
        &nbsp; = one empty space</p>
</asp:Content>



<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <h3 class="code-title-background">Useful codes</h3>
    <asp:DataGrid ID="Code_FrontEnd" runat="server" CssClass="code"
        GridLines="None" Width="500px" CellPadding="2" >
        <HeaderStyle Font-Size="Large" BackColor="#999966" ForeColor="White" />
        <ItemStyle BackColor="#1a1d23" ForeColor="#ffffff"/>     
    </asp:DataGrid>
    
    <p>HTML Sequence</p>
    
    <p>Html tags --> Header tag --> Body tag</p>
    <p>For image use image tag, src tag, alt text</p>
    <p><img class="image-right" src="perce-rock.jpg" width="300" /></p>
</asp:Content>
