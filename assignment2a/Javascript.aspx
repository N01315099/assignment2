﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Javascript.aspx.cs" Inherits="assignment2a.Javascript" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Referlinks" runat="server">

<h3 class="useful-links">Helpful Links</h3>
<div class="link-box">
    <p><a href="https://www.w3schools.com/js/js_functions.asp">Functions</a></p>
    <p><a href="https://www.w3schools.com/js/js_objects.asp">Objects</a></p>
    <p><a href="https://www.w3schools.com/js/js_arrays.asp">Arrays</a></p>
    <p><a href="https://caniuse.com/">Check browser support</a></p>
</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Code" runat="server">
    <h3 class="note-title-background">Notes</h3>
    <p>Add your .js (Javascript file) in the header tag. User alert() to display popup. User console.log() to print something
        in the console. To ask input from user use Prompt(). </p>
    <p>A JavaScript function is a block of code designed to perform a particular task.</p>
    <p>function myFunction(p1, p2) {</br>
    return p1 * p2;              // The function returns the product of p1 and p2</br>
}</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Assignments" runat="server">
    <h3 class="extra-title-background">Assignment Notes</h3>
    <p>A JavaScript function is defined with the function keyword, followed by a name, followed by parentheses ().</br>

Function names can contain letters, digits, underscores, and dollar signs (same rules as variables).</br>

The parentheses may include parameter names separated by commas:
(parameter1, parameter2, ...)

The code to be executed, by the function, is placed inside curly brackets: {}</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <h3 class="code-title-background">Useful codes</h3>
    <p><strong>Using while loop</strong></p>
    <p>
        //CHECK FOR ITEMS UNTIL THRESHOLD IS MET.</br>

        while (sum<35){</br>
	        total= prompt("Enter price");<br />
        }	
        console.log(sum); </br>

</p>
</asp:Content>
