﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="MySQL.aspx.cs" Inherits="assignment2a.MySQL" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Referlinks" runat="server">

<h3 class="useful-links">Helpful Links</h3>
<div class="link-box">
    <p><a href="https://www.oracle.com/technetwork/developer-tools/sql-developer/downloads/sqldev-install-windows-1969674.html">SQL Download</a></p>
    <p><a href="https://www.techonthenet.com/oracle/alias.php">Aliases</a></p>
    <p><a href="http://www.sqlines.com/oracle-to-sql-server/to_char_datetime">TO_CHAR</a></p>
    <p><a href="https://caniuse.com/">Check browser support</a></p>
</div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Code" runat="server">
    <h3 class="note-title-background">Notes</h3>
    <p>TO RUN QUERY Ent+ Ctrl, TO RUN SCRIPT F5</p>
    <p>Start with <em>Select-->from-->where--></em></p
    <%
        /*
        string myhtml = "<html><head></head></html>";
        Response.Write(HttpUtility.HtmlEncode(myhtml));
        */
    %>
    <p>Use || for concatenation, Substr(string,1,3) to select letter from strings </p>
    <p>Order by to order data by ascending or descending</p>
    <p> Every table has a Primary key and to join table it requires foreign key</p>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Assignments" runat="server">
    <h3 class="extra-title-background">Assignment Notes</h3>
    <p>IN operator which can be used instead of OR operator to make multiple OR conditions easier.</p>
    <p>Implicit syntax has where and  explicit has on.Explicit has inner join, left join and right join for easier understanding </p>
    <p>It’s confusing when to use single quotes and double in programming world. But oracle takes single quotes and single ‘=’.</p>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">
    <h3 class="code-title-background">Useful codes</h3>
    <p>select clientfname ||' '||clientlname as fullname, clientphone, clientemail as email, clients.clientid from clients right join invoices on clients.clientid = invoices.clientid where
(invoices.invoice_amount>'1000') or (invoices.invoice_description like '%consultation%') order by clients.clientid asc;</p>
    <p>select make, AVG(production_year) from cars where (make = 'Ford') group by make</p>
</asp:Content>
